//By Rajeshwar Patlolla
//https://github.com/rajeshwarpatlolla/ionic-toast
//rajeshwar.patlolla@gmail.com

'use strict';
angular.module('ionic-toast', ['ionic'])

    .run(['$templateCache', function ($templateCache) {
        var toastTemplate = '<div class="ionic_toast cssFade" ng-show="ionicToast.shown">' +
            '<span ng-bind-html="ionicToast.toastMessage"></span>' +
            '</div>';

        $templateCache.put('ionic-toast/templates/ionic-toast.html', toastTemplate);
    }])

    .provider('ionicToast', function () {

        this.$get = ['$compile', '$document', '$interval', '$rootScope', '$templateCache', '$timeout',
            function ($compile, $document, $interval, $rootScope, $templateCache, $timeout) {

                var defaultScope = {
                    toastMessage: '',
                    shown: false
                };

                var toastTimeout;

                var toastPosition = {
                    top: 'ionic_toast_top',
                    middle: 'ionic_toast_middle',
                    bottom: 'ionic_toast_bottom'
                };

                var toastScope = $rootScope.$new();
                var toastTemplate = $compile($templateCache.get('ionic-toast/templates/ionic-toast.html'))(toastScope);

                toastScope.ionicToast = defaultScope;

                var contentDiv = document.querySelector('ion-side-menu-content') || document.querySelector('ion-content');
                angular.element(contentDiv).append(toastTemplate);

                var toggleToast = function (toState, callback) {
                    toastScope.ionicToast.shown = toState;
                    if (callback) {
                        callback();
                    }
                };

                toastScope.hide = function () {
                    toggleToast(false, function () {
                        console.log('toast hidden');
                    });
                };

                return {

                    show: function (message, position, closeBtn, duration) {

                        if (!message || !position || !duration) return;
                        $timeout.cancel(toastTimeout);
                        if (duration > 5000) duration = 5000;

                        angular.extend(toastScope.ionicToast, {
                            toastMessage: message
                        });

                        toggleToast(true, function () {
                            if (closeBtn)  return;

                            toastTimeout = $timeout(function () {
                                toastScope.hide();
                            }, duration);
                        });
                    },

                    hide: function () {
                        toastScope.hide();
                    }
                };
            }
        ];
    });



